//
//  NSBundle+Language.h
//  EziTaxi
//
//  Created by ABC on 21/03/16.
//  Copyright © 2016 Jigs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (Language)

+(void)setLanguage:(NSString*)language;

@end
