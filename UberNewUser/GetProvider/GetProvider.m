//
//  GetProvider.m
//  TaxiNow
//
//  Created by My Mac on 7/10/15.
//  Copyright (c) 2015 Jigs. All rights reserved.

#import "GetProvider.h"
#import "showdriverCell.h"
#import "ProviderDetailsVC.h"
#import "UIImageView+Download.h"
#import "UIView+Utils.h"
#import "ASStarRatingView.h"
#import "RatingBar.h"

@interface GetProvider()
{
    NSString *strForUserId,*strForUserToken,*strReqId,*strForDriverLatitude,*strForDriverLongitude,*strForIp;
}

@end

@implementation GetProvider
@synthesize arrProviders,strForLatitude,strForLongitude,strForTypeid,timerForCheckReqStatus,strPayment_Option;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [super setBackBarItem];
    strForIp = [self GetOurIpAddress];
    [[AppDelegate sharedAppDelegate]hideLoadingView];
    NSLog(@"arr = %@",arrProviders);
    
    NSSortDescriptor * brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"total_price" ascending:YES];
    NSArray *sortedArray = [arrProviders sortedArrayUsingDescriptors:@[brandDescriptor]];
    arrProviders=[[NSMutableArray alloc]init];
    [arrProviders addObjectsFromArray:sortedArray];
    
    self.btnCancelRequest.hidden = YES;
    self.tblProviderList.contentInset = UIEdgeInsetsZero;
    self.automaticallyAdjustsScrollViewInsets = NO;
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.btnMenu setTitle:NSLocalizedString(@"SELECT_BEAUTICAIN", nil) forState:UIControlStateNormal];
    [self.btnMenu setTitle:NSLocalizedString(@"SELECT_BEAUTICAIN", nil) forState:UIControlStateHighlighted];
    [self.btnCancelRequest setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
    [self.btnCancelRequest setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateHighlighted];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [timerForCheckReqStatus invalidate];
    timerForCheckReqStatus=nil;
    
    self.btnCancelRequest.hidden=YES;
    //[APPDELEGATE.window addSubview:self.btnCancelRequest];
    //[APPDELEGATE.window sendSubviewToBack:self.btnCancelRequest];
    
}

- (NSString *)GetOurIpAddress
{
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
}

#pragma mark
#pragma mark - UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrProviders.count;
}
-(showdriverCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    showdriverCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProviderList"];

    if(cell == nil)
    {
        cell = [[showdriverCell alloc]
                initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"ProviderList"];
    }
    
    cell.lblDriverName.text = [NSString stringWithFormat:@"%@ %@",[[arrProviders objectAtIndex:indexPath.row]valueForKey:@"first_name"],[[arrProviders objectAtIndex:indexPath.row]valueForKey:@"last_name"]];
    cell.lblPrice.text = [NSString stringWithFormat:@"€%@",[[arrProviders objectAtIndex:indexPath.row]valueForKey:@"total_price"]];
    
    NSString *rating = [NSString stringWithFormat:@"%@",[[arrProviders objectAtIndex:indexPath.row]valueForKey:@"rating"]];
    
    [cell.ratingview initRateBar];
    [cell.ratingview setRatings1:([rating floatValue]*2)];
    cell.ratingview.userInteractionEnabled = NO;
    
    [cell.imgDriverProfile downloadFromURL:[[arrProviders objectAtIndex:indexPath.row]valueForKey:@"picture"] withPlaceholder:[UIImage imageNamed:@"user"]];
    [cell.imgDriverProfile applyRoundedCornersFull];
   
    cell.lblRate.text = [NSString stringWithFormat:@"%@",[[arrProviders objectAtIndex:indexPath.row]valueForKey:@"rating"]];
    
    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    strForUserId=[pref objectForKey:PREF_USER_ID];
    strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
    
    NSDictionary *dict=[arrProviders objectAtIndex:indexPath.row];
    NSString *strProviderId=[dict valueForKey:@"id"];
    
    [pref setObject:strProviderId forKey:@"PID"];
    [pref synchronize];
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setValue:@"1" forKey:PARAM_DISTANCE];
    [dictParam setValue:strForLatitude forKey:PARAM_LATITUDE];
    [dictParam setValue:strForLongitude  forKey:PARAM_LONGITUDE];
    [dictParam setValue:strForUserId forKey:PARAM_ID];
    [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
    [dictParam setValue:strForTypeid forKey:PARAM_TYPE];
    [dictParam setValue:strPayment_Option forKey:PARAM_PAYMENT_OPT];
    [dictParam setValue:strProviderId  forKey:@"provider_id"];
    [dictParam setValue:strForIp forKey:@"ip"];
    [dictParam setValue:self.strForAddressNote forKey:@"address_note"];
    
    NSLog(@" half dict = %@",dictParam);
    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"CREATING_REQUEST", nil)];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn getDataFromPath:FILE_CREATE_REQUEST_PROVIDERS withParamData:dictParam withBlock:^(id response, NSError *error)
     {
         // [[AppDelegate sharedAppDelegate]hideLoadingView];
         if (response)
         {
             NSLog(@"res = %@",response);
             response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
             
             if([[response valueForKey:@"success"]boolValue])
             {
                 NSUserDefaults *prefe = [NSUserDefaults standardUserDefaults];
                 NSString *strred=[response valueForKey:@"request_id"];
                 [prefe setObject:strred forKey:PREF_REQ_ID];
                 [prefe synchronize];
                 NSLog(@"req id :%@",strred);
                 [self.btnCancelRequest setHidden:NO];
                 [APPDELEGATE.window addSubview:self.btnCancelRequest];
                 [APPDELEGATE.window bringSubviewToFront:self.btnCancelRequest];
                 
                 timerForCheckReqStatus = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(checkForDriverRequestStatus) userInfo:nil repeats:YES];
             }
             else
             {
                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                 NSString *str1 = [response valueForKey:@"error_code"];
                 if([str1 intValue] == 406)
                 {
                     [timerForCheckReqStatus invalidate];
                     timerForCheckReqStatus = nil;
                     [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                 }
                 else
                 {
                     [APPDELEGATE showToastMessage:[response valueForKey:@"error"]];
                 }
             }
             
         }
     }];
    
}

-(void)checkForDriverRequestStatus
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        strForUserId=[pref objectForKey:PREF_USER_ID];
        strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        strReqId=[pref objectForKey:PREF_REQ_ID];
        NSLog(@" q :%@",strReqId);
        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",FILE_GET_REQUEST,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken,PARAM_REQUEST_ID,strReqId];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue] && [[response valueForKey:@"confirmed_walker"] integerValue]!=0)
                 {
                     NSLog(@"GET REQ--->%@",response);
                     NSString *strCheck=[response valueForKey:@"walker"];
                     
                     if(strCheck)
                     {
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                         NSMutableDictionary *dictWalker=[response valueForKey:@"walker"];
                         strForDriverLatitude=[dictWalker valueForKey:@"latitude"];
                         strForDriverLongitude=[dictWalker valueForKey:@"longitude"];
                         if ([[response valueForKey:@"is_walker_rated"]integerValue]==1)
                         {
                             [pref removeObjectForKey:PREF_REQ_ID];
                             [pref synchronize];
                         }
                         
                         ProviderDetailsVC *vcFeed = nil;
                         for (int i=0; i<self.navigationController.viewControllers.count; i++)
                         {
                             UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:i];
                             if ([vc isKindOfClass:[ProviderDetailsVC class]])
                             {
                                 vcFeed = (ProviderDetailsVC *)vc;
                             }
                         }
                         if (vcFeed==nil)
                         {
                             [timerForCheckReqStatus invalidate];
                             timerForCheckReqStatus=nil;
                             [[AppDelegate sharedAppDelegate]hideLoadingView];
                             
                             self.btnCancelRequest.hidden=YES;
                             //[self.btnCancelRequest removeFromSuperview];
                             //[APPDELEGATE.window sendSubviewToBack:self.btnCancelRequest];
                             
                             [self performSegueWithIdentifier:SEGUE_TO_PROVIDER sender:self];
                         }
                         else
                         {
                             [timerForCheckReqStatus invalidate];
                             timerForCheckReqStatus=nil;
                             [[AppDelegate sharedAppDelegate]hideLoadingView];
                             [self.navigationController popToViewController:vcFeed animated:NO];
                         }
                         
                     }
                 }
                 if([[response valueForKey:@"confirmed_walker"] intValue]==0 && [[response valueForKey:@"status"] intValue]==1)
                 {
                     [[AppDelegate sharedAppDelegate]hideLoadingView];
                     [timerForCheckReqStatus invalidate];
                     timerForCheckReqStatus=nil;
                     NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                     [pref removeObjectForKey:PREF_REQ_ID];
                     
                     if([[response valueForKey:@"is_cancelled"] intValue]==1)
                     {
                         [APPDELEGATE showToastMessage:NSLocalizedString(@"NO_RESPONCE", nil)];
                     }
                     else
                     {
                         [APPDELEGATE showToastMessage:NSLocalizedString(@"NO_WALKER", nil)];
                     }
                     
                     self.btnCancelRequest.hidden=YES;
                     // [self showMapCurrentLocatinn];
                     [APPDELEGATE hideLoadingView];
                 }
                 else
                 {
                     //[APPDELEGATE showToastMessage:[response valueForKey:@"error"]];
                     NSString *str1 = [response valueForKey:@"error_code"];
                     if([str1 intValue] == 406)
                     {
                         [timerForCheckReqStatus invalidate];
                         timerForCheckReqStatus = nil;
                         [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                     }
                 }
             }
             else
             {
                 
             }
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Status" message:@"Sorry, network is not available. Please try again later." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:SEGUE_TO_PROVIDER])
    {
        
        ProviderDetailsVC *obj=[segue destinationViewController];
        obj.strForLatitude=strForLatitude;
        obj.strForLongitude=strForLongitude;
        obj.strForWalkStatedLatitude=strForDriverLatitude;
        obj.strForWalkStatedLongitude=strForDriverLongitude;
        
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



- (IBAction)onClickCancelRequest:(id)sender
{
    if([CLLocationManager locationServicesEnabled])
    {
        if([[AppDelegate sharedAppDelegate]connected])
        {
            [[AppDelegate sharedAppDelegate]hideLoadingView];
            [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"CANCLEING", nil)];
            
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            strForUserId=[pref objectForKey:PREF_USER_ID];
            strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
            NSString *strReqId=[pref objectForKey:PREF_REQ_ID];
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            
            [dictParam setValue:strForUserId forKey:PARAM_ID];
            [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
            [dictParam setValue:strReqId forKey:PARAM_REQUEST_ID];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_CANCEL_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 if (response)
                 {
                     response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                     if([[response valueForKey:@"success"]boolValue])
                     {
                         [timerForCheckReqStatus invalidate];
                         timerForCheckReqStatus=nil;
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                         [self.btnCancelRequest setHidden:YES];
                         //[self.btnCancelRequest removeFromSuperview];
                         [APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_CANCEL", nil)];
                         
                     }
                     else
                     {
                         [APPDELEGATE hideLoadingView];
                         NSString *str1 = [response valueForKey:@"error_code"];
                         if([str1 intValue] == 406)
                         {
                             [timerForCheckReqStatus invalidate];
                             timerForCheckReqStatus = nil;
                             [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                         }
                         //[APPDELEGATE showToastMessage:[response valueForKey:@"error_messages"]];
                     }
                 }
                 
                 
             }];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Status" message:@"Sorry, network is not available. Please try again later." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
        }
        
    }
    else
    {
        UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> Towber -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alertLocation.tag=100;
        [alertLocation show];
        
        
    }
    
}
@end
